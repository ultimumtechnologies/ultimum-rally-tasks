FROM python:3.8
WORKDIR /opt

# Install Rally & requiremets
#RUN apt-get update
#RUN apt-get install -y python3-jsonpath-rw-ext openssh-client curl python3-dev gcc libssl-dev wget
RUN pip3 install pip jsonpath-rw-ext --upgrade
COPY requirements.txt ./
RUN pip3 install -r requirements.txt

RUN chmod 777 /opt && \
    mkdir /etc/rally && \
    chmod 777 /etc/rally

COPY rally.conf /etc/rally/
COPY heat_templates ./heat_templates/
COPY plugins ./plugins/
COPY rally_exporter.py ./
COPY run_rally.sh ./
COPY generate_to_override_tempest.conf.sh ./
COPY logging.conf ./

CMD ./run_rally.sh
