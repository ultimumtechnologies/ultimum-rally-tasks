from rally.task import exporter
from rally.plugins.common.exporters.html import HTMLStaticExporter
from rally.plugins.common.exporters.elastic.flatten import transform as flatten
from requests import post
from datetime import datetime
from copy import deepcopy
from tempfile import NamedTemporaryFile
from subprocess import check_call
from jsonpath_rw_ext import match as json_match
from re import findall as re_findall

@exporter.configure("ultimum")
class ElasticSearchExporter(exporter.TaskExporter):
    @staticmethod
    def _transform_table_output(data):
        data = deepcopy(data)
        data['calculated_outputs'] = {}
        path_query = '$..output.[aditive,complete][?(@.chart_plugin==Table) & (@.data.cols[*]=Key) & (@.data.cols[*]=Value)]'
        selected_items = json_match(path_query, data)
        for item in selected_items:
            index_map = dict((i, item['data']['cols'].index(i)) for i in ['Value', 'Key'])
            append_output = dict((i[index_map['Key']], i[index_map['Value']]) for i in item['data']['rows'])
            ident = re_findall(r'\(#([^\)]+)\)', item.get('description'))
            data['calculated_outputs'][ident[0] if ident else item['title']] = append_output
        return data

    def generate(self):
        cert_path_prefix = '^cert:\/\/[^@]+@[^@]+$'
        html_exporter = HTMLStaticExporter(tasks_results=deepcopy(self.tasks_results), output_destination=None)
        html_data = html_exporter.generate().get("print")
        flat_data = flatten(self._transform_table_output(deepcopy(self.tasks_results[0])))
        flat_zip = [i.rsplit('=', 1) for i in flat_data]
        flat_dict = dict([(k, v if v else None) for k, v in flat_zip])
        data = {"version": "1.0",
                "data": flat_dict,
                "html_report": html_data,
                'html_report_name': datetime.now().isoformat() + ".html"}
        if len(re_findall(cert_path_prefix, self.output_destination)) == 0:
            raise Exception(f"Out parameter must be in format cert://[password]@[path to cert], given {self.output_destination}")
        password, cert = self.output_destination[len('cert://'):].split('@', 2)
        with NamedTemporaryFile() as f_:
            check_call(['openssl', 'pkcs12', '-in', cert, '-passin', 'pass:' + password, '-out', f_.name, '-clcerts', '-nodes'])
            result = post('https://internal.ultimum.io/rally.in', json=data, cert=f_.name, timeout=15)
        result.raise_for_status()
        return {'print': "Data send..."}