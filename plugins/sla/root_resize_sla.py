from rally.task.sla import configure as configure_sla
from abstract_sla import TestSlaAbstract


@configure_sla(name='sla_root_resize_test')
class RootResizeSla(TestSlaAbstract):
    """
    Check if difference between whole disk and filesystem size is less then parameter,
    where parameter is percentage (0-1)
    """
    _sla_failed = "Root disk is not resized"
    _sla_pass = "Root disk is resized"
    _output_identificator = 'root_resize_test'

    def _check(self, items):
        success = True
        p = self.criterion_value
        for item in items:
            success = success and item['disk_size'] * p > item['disk_size'] - item['root_size']
        return success


