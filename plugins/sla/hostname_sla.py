from rally.task.sla import configure as configure_sla
from abstract_sla import TestSlaAbstract


@configure_sla(name='sla_hostname_test')
class HostnameSla(TestSlaAbstract):
    """
    Test if sata test return true
    """
    _sla_failed = "Hostname is not correctly set"
    _sla_pass = "Hostname is correctly set"
    _output_identificator = 'hostname_test'

    def _check(self, items):
        success = True
        for item in items:
            success = success and item['success']
        return success