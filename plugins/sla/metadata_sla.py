from rally.task.sla import configure as configure_sla
from abstract_sla import TestSlaAbstract


@configure_sla(name='sla_metadata_test')
class MetadataSla(TestSlaAbstract):
    """
    Check if all required parameters are present in image
    """
    _sla_failed = "Image metadata is not correctly set"
    _sla_pass = "Image metadata is correctly set"
    _output_identificator = 'metadata_test'

    def _check(self, items):
        success = True
        for item in items:
            success = success and bool(item.get('default_user')) and bool(item.get('default_user_password')) and \
                      bool(item.get('os_distro')) and bool(item.get('os_distro_major')) and \
                      bool(item.get('os_type')) and bool(item.get('root_password')) and \
                      item.get('hw_scsi_model') == "virtio-scsi" and item.get('hw_disk_bus') == "scsi" and \
                      item.get('hw_qemu_guest_agent') == "yes" and item.get('os_require_quiesce') == "yes"
        return success
