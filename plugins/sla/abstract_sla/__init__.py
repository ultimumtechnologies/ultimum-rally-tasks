from rally.task import sla
from abc import abstractmethod

class TestSlaAbstract(sla.SLA):
    CONFIG_SCHEMA = {"type": "number"}
    _sla_failed = "SLA failed"
    _sla_pass = "SLA pass"
    _output_identificator = None

    def __init__(self, criterion_value):
        super(TestSlaAbstract, self).__init__(criterion_value)
        self.success = False

    def add_iteration(self, iteration):
        merged_output = iteration['output']['additive'] + iteration['output']['complete']
        filter_output = [i['data']['rows'] for i in merged_output if i.get('description','').endswith("(#{})".format(self._output_identificator))]
        to_dict = [dict([(k[2], k[1]) for k in i]) for i in filter_output]
        self.success = self._check(to_dict)
        return self.success

    def merge(self, other):
        return self.success and other.success

    def details(self):
        return self._sla_pass if self.success else self._sla_failed

    @abstractmethod
    def _check(self, data):
        pass