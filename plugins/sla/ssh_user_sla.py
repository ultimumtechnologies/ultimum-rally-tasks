from rally.task.sla import configure as configure_sla
from abstract_sla import TestSlaAbstract


@configure_sla(name='sla_ssh_user_test')
class SshUserSla(TestSlaAbstract):
    """
    Test if ssh access by user return true and by root return false
    """
    _sla_failed = "SSH not correctly set for user or root"
    _sla_pass = "SSH configuration is passed"
    _output_identificator = 'ssh_user_test'

    def _check(self, items):
        success = True
        for item in items:
            success = success and item['user_ssh'] and not item['root_ssh'] and not item['user_ssh_password']
        return success