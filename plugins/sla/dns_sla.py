from rally.task.sla import configure as configure_sla
from abstract_sla import TestSlaAbstract


@configure_sla(name='sla_dns_test')
class SataSla(TestSlaAbstract):
    """
    Test if dns test return true
    """
    _sla_failed = "Can't resolve domain name google.com"
    _sla_pass = "Domain names are resolvable"
    _output_identificator = 'dns_test'

    def _check(self, items):
        success = True
        for item in items:
            success = success and item['success']
        return success