from rally.task.sla import configure as configure_sla
from abstract_sla import TestSlaAbstract


@configure_sla(name='sla_sata_test')
class SataSla(TestSlaAbstract):
    """
    Test if sata test return true
    """
    _sla_failed = "SSH not correctly set for user or root"
    _sla_pass = "SSH configuration is passed"
    _output_identificator = 'sata_test'

    def _check(self, items):
        success = True
        for item in items:
            success = success and item['success']
        return success