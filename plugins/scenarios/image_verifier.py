from rally.common import logging
from rally.task import types
from rally.task import validation
from rally_openstack import consts
from rally_openstack.scenario import configure as configure_scenario
from rally_openstack.services.image.glance_v2 import GlanceV2Service as Image
from abstract_scenario.heat_stack_scenario import HeatStackScenario
from abstract_scenario.formatters_scenario import FormattersScenario

LOG = logging.getLogger(__name__)


# Convert params to uuid
@types.convert(flavor={"type": "nova_flavor"})
@types.convert(image={"type": "glance_image"})
@types.convert(public_network={"type": "neutron_network"})
@validation.add("flavor_exists", param_name="flavor")
@validation.add("image_exists", param_name="image", nullable=False)
@validation.add("external_network_exists", param_name="external_network")
@validation.add("image_valid_on_flavor", flavor_param="flavor", image_param="image")
@validation.add("required_platform", platform="openstack", users=True)
@validation.add("required_services",
                services=(consts.Service.NOVA,
                          consts.Service.GLANCE,
                          consts.Service.HEAT,
                          consts.Service.NEUTRON))
@configure_scenario(name="ImageVerifier.check_image_validity",
                    platform="openstack")
class ImageVerificationTest(HeatStackScenario, FormattersScenario):
    template = 'image_verifier.yaml'
    scripts = ['image_verifier.py']

    def run(self, flavor, public_network, image, **kwargs):
        """
        Test if image from image looks correct
        :param dont_test_attribute: name of attribute for skipping image testing
        :param size_volume: size of volume for image boot
        :param public_network: name of public network
        :param flavor: flavor to be used to boot an instance
        :param image_uuid: uuid of image to test
        """
        image_client = Image(
            self._clients, name_generator=self.generate_random_name,
            atomic_inst=self.atomic_actions())
        image_obj = image_client.get_image(image)
        image_name = image_obj.name

        metadata = [
            "default_user",
            "default_user_password",
            "os_distro",
            "os_distro_major",
            "os_type",
            "root_password",
            "hw_scsi_model",
            "hw_disk_bus",
            "hw_qemu_guest_agent",
            "os_require_quiesce"
        ]
        image_data = dict([(k, getattr(image_obj, k, None)) for k in metadata])
        self.output_table_from_dict(title='Image metadata',
                                    description="Image metadata for {}".format(image_name),
                                    identificator='metadata_test',
                                    data=image_data)

        from_stack = self.run_check_stack({
            'image': image,
            'flavor': flavor,
            'public_network': public_network
        })

        root_resize = from_stack.get('root_resize_test')
        self.add_output(complete={"title": "Disk used by root filesystem",
                                  "description": "Percentage of disk used by root filesystem for {}".format(image_name),
                                  "chart_plugin": "Pie",
                                  "data": [
                                      ["Reserved space", root_resize.get('disk_size') - root_resize.get('root_size')],
                                      ["Empty size of root partition",
                                       root_resize.get('root_size') - root_resize.get('root_size_used')],
                                      ["Space used by root filesystem", root_resize.get('root_size_used')]],
                                  })
        self.output_table_from_dict(title='Statistics for disk',
                                    description="Statistics for disk for {}".format(image_name),
                                    data=root_resize,
                                    identificator='root_resize_test',
                                    data_description={'disk_size': 'Size of whole disk',
                                                      'root_size': 'Size reported by filesystem',
                                                      'root_size_used': 'Used space by filesystem'})

        self.output_table_from_dict(title='SSH connection test',
                                    description='Test ssh connection for {}'.format(image_name),
                                    identificator='ssh_user_test',
                                    data=from_stack.get('ssh_user_test'),
                                    data_description={'user_ssh': 'Can remote login to user name (same as os lowecase '
                                                                  'name)?',
                                                      'root_ssh': 'Can remote login to root user (should not be '
                                                                  'possible)?',
                                                      'user_ssh_password': 'Can user log with password?'})

        self.output_table_from_dict(title='Sata test',
                                    description='Sata test for {}'.format(image_name),
                                    identificator='sata_test',
                                    data=from_stack.get('sata_test'),
                                    data_description={'success': 'Is device type correct?'})

        self.output_table_from_dict(title='Hostname test',
                                    description='Hostname test for {}'.format(image_name),
                                    identificator='hostname_test',
                                    data=from_stack.get('hostname_test'),
                                    data_description={'hostname': 'Instance hostname',
                                                      'success': 'Is hostname correct?'})

        self.output_table_from_dict(title='Dns test',
                                    description='Dns test for {}'.format(image_name),
                                    identificator='dns_test',
                                    data=from_stack.get('dns_test'),
                                    data_description={'success': 'Can resolve dns?'})
