import rally.task.utils as rally_utils
from jinja2 import Environment
from jinja2 import FileSystemLoader
from rally.common import logging
from rally.task import types
from rally.task import validation
from rally_openstack import consts
from rally_openstack import scenario

LOG = logging.getLogger(__name__)


@types.convert(image={"type": "glance_image"},
               flavor={"type": "nova_flavor"})
@validation.add("image_valid_on_flavor", flavor_param="flavor",
                image_param="image")
@validation.add("required_platform", platform="openstack", users=True)
@validation.add("required_services",
                services=(consts.Service.NOVA, consts.Service.GLANCE))
@scenario.configure(name="UpdateCheck.check_available_updates",
                    platform="openstack")
class UpdateCheck(scenario.OpenStackScenario):

    def run(self, image, flavor, **kwargs):
        """
        :param image: image to be used to boot an instance
        :param flavor: flavor to be used to boot an instance
        """

        # Template generating

        j2_env = Environment(
            loader=FileSystemLoader('plugins/scenarios/heat_templates'),
            trim_blocks=True)
        j2_template = j2_env.get_template('apt_updates_template.j2')
        rendered_template = j2_template.render(image=image, flavor=flavor)

        stack_name = self.generate_random_name()

        kw = {
            "stack_name": stack_name,
            "disable_rollback": True,
            "parameters": {},
            "template": rendered_template,
            "files": {},
            "environment": {}
        }

        stack_id = self.clients("heat").stacks.create(**kw)["stack"]["id"]
        stack = self.clients("heat").stacks.get(stack_id)

        self.sleep_between(2.0)

        stack = rally_utils.wait_for_status(
            stack,
            ready_statuses=["CREATE_COMPLETE"],
            failure_statuses=["CREATE_FAILED", "ERROR"],
            update_resource=rally_utils.get_from_manager(),
            timeout=3600.0,
            check_interval=1.0)

        total_packages_to_install = ''
        for element in stack.outputs:
            if element['output_key'] == 'packages_total':
                total_packages_to_install = element['output_value']

        self.add_output(
            additive={
                "title": "Available updates",
                "description": "Number of upgradable packages",
                "chart_plugin": "StackedArea",
                "data": [["total_packages_to_install", float(total_packages_to_install)]]
            }
        )

        stack.delete()
