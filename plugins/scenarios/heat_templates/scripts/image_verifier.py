#!/usr/bin/env python3
from os import environ, chmod, path
from shlex import split as shsplit
from subprocess import run, check_call, check_output, Popen, PIPE, STDOUT, CalledProcessError
from json import dumps
from re import search
from socket import gethostbyname
import six
import time


# Notify for wait condition
def notify(data, status=True):
    payload = {'status': 'SUCCESS' if status else 'FAILURE',
               'data': data}
    cmd = shsplit(environ['WC_NOTIFY']) + ['--data-binary', dumps(payload)]
    check_call(cmd)
    check_call(['echo'] + cmd)


# Check if difference between disk and resized fs partition is less then 15% (because of reserved space on partition)
disk_size_lines = check_output(['fdisk', '-l', '/dev/disk/by-label/cloudimg-rootfs'])
if isinstance(disk_size_lines, six.binary_type):
    disk_size_lines = disk_size_lines.decode('utf-8')
match = search(r"Disk\s+[^:]+:\s+[^,]+,\s+([0-9]+) bytes.*", disk_size_lines)
disk_size = int(match.group(1))
root_size = int(check_output(['df', '--output=size', '-B1', '/']).splitlines()[1])
root_size_used = int(check_output(['df', '--output=used', '-B1', '/']).splitlines()[1])
notify({"disk_size": disk_size, "root_size": root_size, "name": "root_resize_test", "root_size_used": root_size_used})

# Check if disk is sata
notify({'name': 'sata_test', 'success': path.exists('/dev/sda')})

# Check hostname
hostname = check_output(['hostname']).strip()
if isinstance(hostname, six.binary_type):
    hostname = hostname.decode('utf-8')
notify({'name': 'hostname_test', 'hostname': hostname, 'success': hostname == 'test-verify-image'})

# Check login to local user and then to root user
with open('/tmp/priv.ssh', 'w') as f_:
    f_.write(environ['SSH_PRIV'])
chmod('/tmp/priv.ssh', 0o600)

ssh_local = ['ssh', '-o', 'GlobalKnownHostsFile=/dev/null', '-o', 'UserKnownHostsFile=/dev/null', '-o',
             'StrictHostKeyChecking=no', '-i', '/tmp/priv.ssh']
ssh_user_and_command = ['test-verify-image', 'echo -n OK']
distro_user = check_output(['lsb_release', '-i'], universal_newlines=True).split(':')[1].strip().lower()

try:
    user_ssh = check_output(ssh_local + ['-l', distro_user] + ssh_user_and_command)
    if isinstance(user_ssh, six.binary_type):
        user_ssh = user_ssh.decode('utf-8')
except CalledProcessError as e:
    if isinstance(e.output, six.binary_type):
        user_ssh = e.output.decode('utf-8')
try:
    root_ssh = check_output(ssh_local + ['-l', 'root'] + ssh_user_and_command, stderr=STDOUT)
    if isinstance(root_ssh, six.binary_type):
        root_ssh = root_ssh.decode('utf-8')
except CalledProcessError as e:
    if isinstance(e.output, six.binary_type):
        root_ssh = e.output.decode('utf-8')

process = Popen(ssh_local + ['-o', 'PreferredAuthentications=none', '-l', distro_user] + ssh_user_and_command, stderr=PIPE)
lines = [l for l in process.stderr.read().decode().splitlines() if 'Permission denied' in l]
if len(lines) != 1:
    raise Exception("Can't check ssh user password settings")

notify({'name': 'ssh_user_test', 'user_ssh': user_ssh == 'OK', 'root_ssh': root_ssh == 'OK', 'user_ssh_password': 'password' in lines[0]})

# Check if google.com name can be resolved
try:
    gethostbyname('google.com')
    hostresolve_ok = True
except:
    hostresolve_ok = False
finally:
    notify({'name': 'dns_test', 'success': hostresolve_ok})
