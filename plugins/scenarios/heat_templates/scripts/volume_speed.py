#!/usr/bin/env python3
import collections
import re
from os import environ, listdir
from shlex import split as shsplit
from subprocess import check_call, check_output
from json import dumps


# Notify for wait condition
def notify(data, status=True):
    payload = {'status': 'SUCCESS' if status else 'FAILURE', 'data': data}
    cmd = shsplit(environ['WC_NOTIFY']) + ['--data-binary', dumps(payload)]
    check_call(cmd)
    check_call(['echo'] + cmd)


def get_disk_by_type():
    disk_id_path = '/dev/disk/by-id/'
    definition = eval(environ['VOLUMES'])
    definition = dict(zip(definition['types'], definition['ids']))
    for disk in listdir(disk_id_path):
        _, uuid_part = disk.rsplit('_', 1)
        uuids = [k for (k, v) in definition.items() if v.startswith(uuid_part)]
        if len(uuids) == 1:
            definition[uuids[0]] = disk_id_path + disk
    if len([i for i in definition.values() if not i.startswith('/dev')]) > 0:
        raise Exception("Cannot map volume id to type")
    return definition


def save_values(start_index, values, result_dict):
    for k, v in result_dict.items():
        if type(v) == list:
            end_index = start_index + len(v)
            if end_index > len(values):
                return start_index, result_dict
            values_to_zip = values[start_index:end_index]
            r = re.compile('\d+\.?\d*%=\d')
            values_match = list(filter(r.match, values_to_zip))
            if values_match:
                v = ['{}th percentile'.format(item.split('%=')[0])
                     for item in values_match]
                values_to_zip = ['{}'.format(item.split('%=')[1])
                                 for item in values_match]
            zip_values = collections.OrderedDict(zip(v, values_to_zip))
            result_dict.update([(k, zip_values)])
            start_index = end_index
        elif type(v) == collections.OrderedDict:
            start_index, _ = save_values(start_index, values, v)
    return start_index, result_dict


def get_fio_header():
    fio_job_info = ['terse version', 'fio version', 'jobname', 'groupid',
                    'error']
    fio_rw_status = ['Total I/O (KB)', 'bandwidth (KB/s)', 'IOPS',
                     'runtime (ms)']
    fio_latency = ['min', 'max', 'mean', 'standard deviation']
    fio_percentile = ['{}th percentile' for i in range(0, 20)]
    fio_bandwidth = ['min', 'max', 'aggregate percentage of total', 'mean',
                     'standard deviation']
    fio_cpu_usage = ['user', 'system', 'context switches', 'major page faults',
                     'minor page faults']
    fio_io_depth_distribution = ['<=1', '2', '4', '8', '16', '32', '>=64']
    fio_io_latency_distribution_us = ['<=2', '4', '10', '20', '50', '100',
                                      '250',
                                      '500', '750', '1000']
    fio_io_latency_distribution_ms = ['<=2', '4', '10', '20', '50', '100',
                                      '250',
                                      '500', '750', '1000', '2000', '>=2000']
    fio_disk_utilization = ['name', 'read ios', 'write ios', 'read merges',
                            'write merges', 'read ticks', 'write ticks',
                            'in-queue time', 'disk utilization percentage']
    fio_error_info = ['total # errors', 'first error code']
    fio_status = collections.OrderedDict(
        [('Status', fio_rw_status),
         ('Submission latency', fio_latency),
         ("Completion latency", fio_latency),
         ("Completion latency percentiles (20 fields)", fio_percentile),
         ('Total latency', fio_latency),
         ('Bandwith', fio_bandwidth)])
    fio_io_latency_distribution = collections.OrderedDict(
        [('Microseconds', fio_io_latency_distribution_us),
         ('Milliseconds', fio_io_latency_distribution_ms)])
    fio_header = collections.OrderedDict([
        ('Job info', fio_job_info),
        ('Read status', fio_status.copy()),
        ('Write status', fio_status.copy()),
        ('CPU usage', fio_cpu_usage),
        ('IO depth distribution', fio_io_depth_distribution),
        ('IO latency distribution', fio_io_latency_distribution.copy()),
        ('Disk utilization', fio_disk_utilization),
        ('Error info', fio_error_info)])
    return fio_header


definition = get_disk_by_type()
for disk_type, disk in definition.items():
    check_call(['sudo', 'mkfs.ext4', disk])
    mount_path = '/mnt/' + disk_type
    check_call(['sudo', 'mkdir', '-p', mount_path])
    check_call(['sudo', 'mount', disk, mount_path])
    result = check_output(['sudo', 'fio', '/opt/config.fio', '--minimal',
                           '--directory={}'.format(mount_path)
                           ]).decode().strip().split(';')
    result_dict = get_fio_header()
    _, result_dict = save_values(0, result, result_dict)
    notify({'name': '{}'.format(disk_type),
            'value': result_dict})
