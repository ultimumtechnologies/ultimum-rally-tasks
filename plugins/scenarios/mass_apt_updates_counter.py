import rally.task.utils as rally_utils
from jinja2 import Environment
from jinja2 import FileSystemLoader
from rally.common import logging
from rally.task import types
from rally.task import validation
from rally_openstack import consts
from rally_openstack import scenario
from rally_openstack.services.image import image

LOG = logging.getLogger(__name__)


@types.convert(flavor={"type": "nova_flavor"})
@validation.add("flavor_exists", param_name="flavor")
@validation.add("required_platform", platform="openstack", users=True)
@validation.add("required_services",
                services=(consts.Service.NOVA, consts.Service.GLANCE))
@scenario.configure(name="UpdateCheck.mass_check_available_updates",
                    platform="openstack")
class UpdateCheck(scenario.OpenStackScenario):

    def run(self, dont_test_attribute, flavor, **kwargs):
        """
        :param dont_test_attribute: attribute for testing
        :param flavor: flavor to be used to boot an instance
        """

        image_client = image.Image(
            self._clients, name_generator=self.generate_random_name,
            atomic_inst=self.atomic_actions())

        image_list = []

        for e_i in image_client.list_images():
            glance_imgs = self._clients.glance("2").images.get(e_i.id)
            if e_i.visibility == 'public':
                if hasattr(glance_imgs, dont_test_attribute) and getattr(
                        glance_imgs, dont_test_attribute) == 'yes':
                    continue
                else:
                    image_list.append({
                        "name": e_i.name,
                        "id": e_i.id
                    })

        for e_i in image_list:
            LOG.info("* Processing image: {}".format(e_i["name"]))

            # Template generating

            j2_env = Environment(loader=FileSystemLoader(
                'plugins/scenarios/heat_templates'),
                trim_blocks=True)
            j2_template = j2_env.get_template('apt_updates_template.j2')
            rendered_template = j2_template.render(image=e_i["id"],
                                                   flavor=flavor)

            stack_name = self.generate_random_name()

            kw = {
                "stack_name": stack_name,
                "disable_rollback": True,
                "parameters": {},
                "template": rendered_template,
                "files": {},
                "environment": {}
            }

            stack_id = self.clients("heat").stacks.create(**kw)["stack"]["id"]
            stack = self.clients("heat").stacks.get(stack_id)

            self.sleep_between(2.0)

            stack = rally_utils.wait_for_status(
                stack,
                ready_statuses=["CREATE_COMPLETE"],
                failure_statuses=["CREATE_FAILED", "ERROR"],
                update_resource=rally_utils.get_from_manager(),
                timeout=3600.0,
                check_interval=1.0)

            total_packages_to_install = ''
            for element in stack.outputs:
                if element['output_key'] == 'packages_total':
                    total_packages_to_install = element['output_value']

            self.add_output(
                additive={
                    "title": "Available updates for " + e_i["name"],
                    "description": "Number of upgradable packages",
                    "chart_plugin": "StackedArea",
                    "data": [["available_packages@" + e_i["name"],
                              float(total_packages_to_install)]]
                }
            )

            stack.delete()
