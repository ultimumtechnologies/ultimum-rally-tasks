from six import iteritems
from rally_openstack import scenario


class FormattersScenario(scenario.OpenStackScenario):
    def output_table_from_dict(self, title, description, data, identificator=None, data_description=None):
        if data_description is None:
            data_description = {}
        if identificator:
            description = "{} (#{})".format(description, identificator)
        self.add_output(complete={"title": title,
                                  "description": description,
                                  "chart_plugin": "Table",
                                  "data": {"cols": ['Description', 'Value', 'Key'],
                                           "rows": [[data_description.get(k, k), v, k] for k, v in iteritems(data)]}})
