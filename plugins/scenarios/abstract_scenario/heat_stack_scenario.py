from rally_openstack import scenario
from rally_openstack.services.heat.main import Stack
from rally.task import utils as rally_utils
from json import loads as json_load
from os.path import dirname, realpath, join as path_join
from os import listdir

class HeatStackScenario(scenario.OpenStackScenario):
    """
    Abstract class that contain basic method for call test via heat stack.
    Assumed that you provide stack file in static template variable. Heat stack need to be self contain
    and provide all exported results in output variable rally_data with structure from wait condition back call.
    e.g.:
    {"1": {"name": "some_name", "first_key": "data", "second_key": "data"},
     "2": {"name": "another_name", "first_key": "data", "second_key": "data"}}
    """
    template = None
    files = ['network_framework.yaml']
    scripts = []
    def _delete_stack(self, stack):
        stack.delete()
        rally_utils.wait_for_status(
            stack,
            ready_statuses=["deleted"],
            check_deletion=True,
            update_resource=rally_utils.get_from_manager(),
            timeout=300.0,
            check_interval=2.0
        )

    def run_check_stack(self, params):
        """
        Create stack and after data is returned make sure that stack is deleted. Merge data from all wait condition
        signals and return them as dict
        @param params: dict input parameters for heat stack
        @return: dict export key from name in dict from stack and set everything else as value
        """
        # get heat path relative to this file
        heat_dir = realpath(path_join(dirname(realpath(__file__)), '../heat_templates'))
        files = {}
        for template in listdir(path_join(heat_dir, 'shared')):
            if template.endswith('.yaml'):
                files[template] = path_join(heat_dir, 'shared', template)
        for script in self.scripts:
            files['scripts/' + script] = path_join(heat_dir, 'scripts', script)

        heat = Stack(scenario=self, task=self.task, template=path_join(heat_dir, self.template), files=files, parameters=params)
        data = {}
        try:
            heat.create()
            data = heat.stack.outputs
        except Exception as e:
            # Rally rewrite crashed task so only possibility to throw error is abort
            self.task.abort()
            raise
        finally:
            self._delete_stack(heat.stack)

        merged_data = {}
        for items in data:
            if items.get('output_key') == 'rally_data':
                for data_parts in json_load(items.get('output_value', "{}")).values():
                    name = data_parts['name']
                    del data_parts['name']
                    merged_data[name] = data_parts
        return merged_data
