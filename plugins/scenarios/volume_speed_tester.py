from rally.common import logging
from rally.task import types
from rally.task import validation
from rally_openstack import consts
from rally_openstack import scenario
from abstract_scenario.heat_stack_scenario import HeatStackScenario
from abstract_scenario.formatters_scenario import FormattersScenario

LOG = logging.getLogger(__name__)


@types.convert(image={"type": "glance_image"},
               flavor={"type": "nova_flavor"})
@types.convert(public_network={"type": "neutron_network"})
@validation.add("image_valid_on_flavor", flavor_param="flavor",
                image_param="image")
@validation.add("required_platform", platform="openstack", users=True)
@validation.add("required_services",
                services=(consts.Service.NOVA, consts.Service.GLANCE))
@scenario.configure(name="VolumeSpeedTest.test_volume_speed",
                    platform="openstack")
class VolumeSpeedTest(HeatStackScenario, FormattersScenario):
    template = 'volume_speed.yaml'
    scripts = ['volume_speed.py', 'config.fio']

    def run(self, image, flavor, public_network, **kwargs):
        """
        :param image: image to be used to boot an instance
        :param flavor: flavor to be used to boot an instance
        """
        from_stack = self.run_check_stack({
            'image': image,
            'flavor': flavor,
            'public_network': public_network,
            'volume_types': "Basic,Basic2"
        })
        for type, value in from_stack.items():
            self.output_table_from_dict(title="Speed test for {}".format(type),
                                        description=
                                        "Speed test performed by fio",
                                        data=value.get('value'),
                                        identificator='speed_{}'.format(type))
        return
