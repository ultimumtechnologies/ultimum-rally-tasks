#!/bin/bash

set -x

RALLY_ARGS="--config-file /etc/rally/rally.conf"
WORKDIR=${WORKDIR:-/opt/}

if [ "$1" = "--args" ]; then
#  {image_name: Ubuntu 16.04, flavor_name: Memory Standard, flavor_name_bigger: Memory Large, availability_zone: nova, image_uuid: 62348796-3e28-479e-bed6-f094d1af99f0}
echo "$2" > ${WORKDIR}/env/task_args.json
fi

cd /opt
mkdir -p /opt/env

if [ -z "$CLOUD_NAME" ]; then
    echo "No CLOUD_NAME given. Exiting..."
    exit 1
fi

if ! [ -z $FLUENT_LOGGING_TAG ] && ! [ -z $FLUENT_LOGGING_HOST ]; then
    if [ -z $FLUENT_LOGGING_LEVEL ]; then
        FLUENT_LOGGING_LEVEL=WARNING
    fi
    cp logging.conf /etc/rally/
    sed -iE s/FLUENT_LOGGING_TAG/$FLUENT_LOGGING_TAG/ /etc/rally/logging.conf
    sed -iE s/FLUENT_LOGGING_HOST/$FLUENT_LOGGING_HOST/ /etc/rally/logging.conf
    sed -iE s/FLUENT_LOGGING_LEVEL/$FLUENT_LOGGING_LEVEL/ /etc/rally/logging.conf
    sed -i '2ilog_config_append = /etc/rally/logging.conf' /etc/rally/rally.conf
fi

if ! [ -f /etc/rally/database.db ]; then
    rally ${RALLY_ARGS} db create
fi

if [ -r /opt/source.rc ]; then
  source /opt/source.rc
  echo "Source OS from /opt/source.rc"
elif [[ -n "$SOURCE_FILE_CONTENT" ]]; then
  echo "${SOURCE_FILE_CONTENT}" | tr -s "$" > /opt/source.rc
  source /opt/source.rc
  echo "Source OS from env var SOURCE_FILE_CONTENT"
elif [[ -n "$OS_AUTH_URL" ]]; then
  echo "Exist env var OS_AUTH_URL, OS sourced"
else
  echo "No .rc file or env var SOURCE_FILE_CONTENT given, or OS didn't source. Exiting..."
  exit 1
fi

#create json file, because create deployment --fromenv create normal user as admin
cat << EOF > ${WORKDIR}/env/cloud.json
{
    "openstack": {
        "auth_url": "$OS_AUTH_URL",
        "region_name": "RegionOne",
        "endpoint_type": "public",
        "users": [
             {
                 "username": "$OS_USERNAME",
                 "password": "$OS_PASSWORD",
                 "user_domain_name": "Default",
                 "project_name": "$OS_PROJECT_NAME",
                 "project_domain_name": "Default"
             }
        ]
    }
}
EOF

RET_VAL=$?
if [ $RET_VAL -eq 0 ]; then
  echo "Successfully sourced OpenStack cloud."
else
  echo "Error in source file or env var of content source file"
  exit 1
fi

if [ -n "$TASKS" ]; then
  HTML_REPORT_FILENAME=rally-report.html
  JSON_REPORT_FILENAME=rally-report.json
  REPORT_OUTPUT_DIR=${WORKDIR}/outputs/
  mkdir -p ${REPORT_OUTPUT_DIR}

  if [ "$TASKS" != "tempest" ]; then
    rally ${RALLY_ARGS} deployment create --filename=${WORKDIR}/env/cloud.json --name ${CLOUD_NAME}
    TASK_FILENAME=/opt/plugins/tasks/openstack_metrics/${TASKS}.yaml

    if ! [ -f "$TASK_FILENAME" ]; then
        echo "No such file ${TASK_FILENAME}"
        exit 1
    fi

    if [ -n "$DEBUG" ]; then
        LOGDIR=${WORKDIR}/env/logs
        LOGFILE=${LOGDIR}/${TASKS}.log
        mkdir -p ${LOGDIR}
        rally ${RALLY_ARGS} -d --plugin-paths /opt/plugins/ task start ${TASK_FILENAME} --task-args-file ${WORKDIR}/env/task_args.json |& tee -a ${LOGFILE}
    else
        rally ${RALLY_ARGS} --plugin-paths /opt/plugins/ task start ${TASK_FILENAME} --task-args-file ${WORKDIR}/env/task_args.json
    fi

    #rally ${RALLY_ARGS} task report --out=${REPORT_OUTPUT_DIR}/${HTML_REPORT_FILENAME}
    #rally ${RALLY_ARGS} task report --json | /opt/rally_exporter.py --report-file ${HTML_REPORT_FILENAME}

    rally ${RALLY_ARGS} --plugin-paths /opt/plugins/ task export --type ultimum --to cert://${CLIENT_CERT_PASS}@${CLIENT_CERT_FILE}

    #Check if task finished correctly and write details
    rally task list 2>/dev/null
    ! ( rally task list 2>/dev/null | head -n -1|tail -n +4 | grep -qv finished )
    TASK_RET_CODE=$?

    # Reset RET_CODE if we skiping sla failure
    if [ -n "$SKIP_PROPAGATE_TASK_FAILURE" ]; then
      TASK_RET_CODE=0
    fi

    #Check if sla passed
    rally ${RALLY_ARGS} task sla-check 2> /dev/null
    SLA_RET_CODE=$?

    # Reset RET_CODE if we skiping sla failure
    if [ -n "$SKIP_PROPAGATE_SLA_FAILURE" ]; then
      SLA_RET_CODE=0
    fi

    # If some error was found raise it
    if [ $((TASK_RET_CODE + SLA_RET_CODE)) -eq 0 ]; then
      exit 0
    else
      exit 1
    fi

  elif [ "$TASKS" == "tempest" ]; then
    rally ${RALLY_ARGS} deployment create --fromenv --name ${CLOUD_NAME}
    rally ${RALLY_ARGS} verify create-verifier --name ${CLOUD_NAME}-verifier --type tempest
    VERIFIER_ID="$(rally ${RALLY_ARGS} verify list-verifiers 2>/dev/null | grep ${CLOUD_NAME}-verifier | tr -d " " | cut -d "|" -f2)"
    DEPLOYMENT_ID="$(rally ${RALLY_ARGS} deployment list 2>/dev/null | grep ${CLOUD_NAME} | tr -d " " | cut -d "|" -f2)"
    rally ${RALLY_ARGS} deployment use --deployment ${DEPLOYMENT_ID}

    rally ${RALLY_ARGS} verify configure-verifier
    ./generate_to_override_tempest.conf.sh
    rally ${RALLY_ARGS} verify configure-verifier --extend to_override_tempest.conf

    wget http://download.cirros-cloud.net/0.4.0/cirros-0.4.0-x86_64-disk.img

    rally ${RALLY_ARGS} verify start --id ${VERIFIER_ID}
    rally ${RALLY_ARGS} verify report --type html-static --to ${REPORT_OUTPUT_DIR}/${HTML_REPORT_FILENAME}
    rally ${RALLY_ARGS} verify report --to ${REPORT_OUTPUT_DIR}/${JSON_REPORT_FILENAME}

  fi
else
  echo "Env variable TASKS is empty."
  exit 1
fi
