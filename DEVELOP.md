For debuging

Only for pycharm-professional (if needed ask for licence)

Got to docker-compose.yaml and change mount point of credential to real location where you credentials are located
Note: on Ubuntu 19.10 was problem where credentials are in encrypted dir (with cryptfs, luks are OK )

Then go to https://ultimumtechnologies.atlassian.net/wiki/spaces/ID/pages/626917377/Create+running+configuration

Example configurations for jobs are (all with module name rally.cli.main ) :

For creating special task_arg file, you can edit parameter to docker-compose

simple testing job:
    --config-file /etc/rally/rally.conf --plugin-paths /opt/project/plugins/ task start /opt/project/plugins/tasks/openstack_metrics/test.yaml --task-args-file /opt/env/task_args.json --deployment testing
    
list all finished jobs:
    --config-file /etc/rally/rally.conf --plugin-paths /opt/project/plugins/ task list --deployment testing
    
Image testing:
    --config-file /etc/rally/rally.conf --plugin-paths /opt/project/plugins/ task start /opt/plugins/tasks/openstack_metrics/image_verifier.yaml --task-args-file /opt/env/task_args.json --deployment testing