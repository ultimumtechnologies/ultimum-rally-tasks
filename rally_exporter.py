#!/usr/bin/python

import abc
import json
import logging
import os
import re
import six
import sys
import time
from pprint import pformat

import elasticsearch
from fluent import handler
from oslo_log.formatters import FluentFormatter
from logging import StreamHandler, Formatter


LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)

console_handler = StreamHandler(sys.stderr)
console_handler.setFormatter(
    Formatter("%(asctime)s %(levelname)s %(message)s"))
console_handler.setLevel(logging.INFO)
LOG.addHandler(console_handler)

flog_tag = os.environ.get("FLUENT_LOGGING_TAG")
flog_host = os.environ.get("FLUENT_LOGGING_HOST")
flog_level = os.environ.get("FLUENT_LOGGING_LEVEL", "INFO")
_flog_level = logging.getLevelName(flog_level)

if flog_tag and flog_host:
    flog_handler = handler.FluentHandler(flog_tag, flog_host)
    flog_handler.setFormatter(FluentFormatter())
    flog_handler.setLevel(_flog_level)
    LOG.addHandler(flog_handler)


@six.add_metaclass(abc.ABCMeta)
class Exporter(object):
    @abc.abstractmethod
    def index_event(self, event_data):
        pass


class ElasticExporter(Exporter):
    def __init__(self, es_uri, es_index, index_settings=None):
        self.es = None
        self.es_url = es_uri
        self.es_index = es_index
        self.document_type = "rally"
        self.index_settings = index_settings
        self._verify_es_connection()
        self._ensure_mapping()

    @property
    def es_connection(self):
        if not self.es:
            self.es = elasticsearch.Elasticsearch([self.es_url])
        return self.es

    def index_event(self, event_data):
        # converting rally timestamp to milis fo elastic search
        timestamp = int(round(event_data["timestamp"] * 1000))
        event_data["timestamp"] = timestamp
        LOG.debug("Indexing event: {}".format(pformat(event_data)))
        try:
            _id = "{}_{}_{}".format(event_data["type"], event_data["title"],
                                    event_data["timestamp"])
            self.es_connection.create(self.es_index, self.document_type,
                                      _id, event_data)
        except Exception:
            LOG.exception("Unable to index event: {}".format(event_data))

    def _verify_es_connection(self):
        self.es_connection.ping()

    def _ensure_mapping(self):
        try:
            self.es_connection.indices.get_mapping(self.es_index)
        except elasticsearch.NotFoundError:
            LOG.debug("No mapping found for index {}.".format(self.es_index))
            if self.index_settings and os.path.isfile(self.index_settings):
                with open(self.index_settings, "r") as settings_file:
                    settings = json.loads(settings_file.read())
                self.es_connection.indices.create(self.es_index, settings)
                LOG.info("Mapping initialized.")
            else:
                LOG.info("No mapping file provided - using dynamic mapping.")


class FluentExporter(Exporter):
    def __init__(self, fluent_tag="openstack.rally_tasks",
                 fluent_host="localhost", fluent_port=24224):
        self.logger = logging.getLogger("RALLY_OUTPUT")
        self.fluent_handler = handler.FluentHandler(fluent_tag,
                                                    fluent_host,
                                                    fluent_port)
        self.logger.addHandler(self.fluent_handler)

    def index_event(self, event_data):
        LOG.debug("Indexing event: {}".format(pformat(event_data)))
        timestamp = event_data.pop(
            "timestamp") if "timestamp" in event_data else time.time()
        self.fluent_handler.sender.emit_with_time(None, timestamp, event_data)


class DummyExporter(Exporter):
    def index_event(self, event_data):
        LOG.info("Indexing event: {}".format(pformat(event_data)))


class RallyDenormalizer(object):
    def __init__(self, exporter, report_uri=None, tasks_name=None):
        self.report_url = report_uri
        self.raw_data = None
        self.title_matcher = re.compile(r"^(?P<section>[^\.\s]+)")
        self.exporter = exporter
        self.tasks_name = tasks_name
        LOG.debug(self.report_url)

    def process_result_json(self, json_input):
        self._load(json_input)
        self._process_data()

    def _load(self, json_input):
        # TODO(janvondra): handle changes in different versions
        self.raw_data = json_input

    def _denormalize_data(self):
        for subtask in self.raw_data['tasks'][0]['subtasks']:
            for workload in subtask['workloads']:
                for context_result in workload['contexts_results']:
                    context_result.pop('plugin_cfg')

    def _process_data(self):
        """Denormalize event documents"""
        for task in self.raw_data["tasks"]:
            self._process_task(task)

    def _process_task(self, task):
        task_data = {
            "env_name": task["env_name"],
            "task_pass_sla": task["pass_sla"]
        }
        if task["title"]:
            task_data["task_title"] = task["title"]
        if task["description"]:
            task_data["task_description"] = task["description"]
        if self.report_url:
            task_data["report_url"] = self.report_url
        if self.tasks_name:
            task_data["tasks_name"] = self.tasks_name

        for subtask in task["subtasks"]:
            self._process_subtask(subtask, task_data)

    def _process_subtask(self, subtask, parent_data):
        subtask_data = parent_data.copy()
        subtask_data["subtask_title"] = subtask['title']
        if subtask['description']:
            subtask_data["subtask_description"] = subtask['description']

        for workload in subtask["workloads"]:
            self._process_workload(workload, subtask_data)

    def _process_workload(self, workload, parent_data):

        workload_data = parent_data.copy()
        # workload_data["scenario_title"] = next(iter(workload["scenario"]))
        workload_data["scenario_title"] = next(iter(workload["scenario"]))
        workload_data["description"] = workload["description"]
        # converting rally timestamp to milis fo elastic search
        if not workload["data"]:
            LOG.error("Unable to parse event: {}".format(workload))
            return
        else:
            timestamp = workload["data"][0]["timestamp"]
        workload_data["timestamp"] = timestamp
        section_name = self._parse_section_from_title(
            next(iter(workload["scenario"])))
        workload_data["section"] = section_name
        scenario = next(iter(workload["scenario"]))
        scenario_details = list(workload["scenario"].values())
        try:
            for detail in scenario_details:
                for k, v in detail.items():
                    if type(v) is dict:
                        for _k, _v in v.items():
                            if type(_v) is not dict:
                                workload_data["{}_{}".format(k, _k)] = _v
                    else:
                        workload_data[k] = v
        except Exception:
            LOG.exception(
                "Unable to get scenario details for {}".format(scenario))

        workload_event = workload_data.copy()
        workload_event["type"] = "workload"
        # TODO(janvondra): handle other values than duration
        statistics = workload["statistics"]["durations"]["total"]["data"]
        workload_event["title"] = scenario
        workload_event["iterations"] = statistics["iteration_count"]
        workload_event["duration_avg"] = statistics["avg"]
        workload_event["duration_median"] = statistics["median"]
        workload_event["duration_min"] = statistics["min"]
        workload_event["duration_max"] = statistics["max"]

        workload_event_copy = workload_event.copy()
        for element in workload["data"]:
            for data_element in element["output"]["additive"]:
                additive_data = workload_event_copy.copy()
                additive_data["type"] = "additive_data"
                additive_data["data_title"] = data_element["title"]
                for additive in data_element["data"]:
                    if '@' in additive[0]:
                        k, v = additive[0].split('@', 1)
                        additive_data["data_identifier"] = v
                        additive_data[k] = additive[1]
                        self.exporter.index_event(additive_data)
                    else:
                        additive_data[additive[0]] = additive[1]
                        self.exporter.index_event(additive_data)
                    workload_event[additive[0]] = additive[1]

        self.exporter.index_event(workload_event)
        # atomic_statistics = {a.pop("name"): a for a in
        #                      workload["statistics"]["durations"][
        #                          "atomics"]}
        # if len(atomic_statistics.keys()) > 1:
        #     for k in atomic_statistics.keys():
        #         self._process_atomic(k, atomic_statistics[k]["data"],
        #                              workload_data)

    def _process_atomic(self, title, statistics, parent_info):
        atomic_data = parent_info.copy()
        atomic_data["title"] = title
        atomic_data["type"] = "atomic"
        atomic_data["iterations"] = statistics["iteration_count"]
        atomic_data["duration_avg"] = statistics["avg"]
        atomic_data["duration_median"] = statistics["median"]
        atomic_data["duration_min"] = statistics["min"]
        atomic_data["duration_max"] = statistics["max"]
        self.exporter.index_event(atomic_data)

    def _parse_section_from_title(self, title):
        m = re.search(self.title_matcher, title)
        return m.group("section") if m else None


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--report-file', help='HTML report filename')
    parser.add_argument('--debug', action='store_true',
                        help='Do not export data. Print to stdout instead.')
    args = parser.parse_args()

    report_file = args.report_file
    debug = args.debug
    reports_url_prefix = os.environ.get("RALLY_REPORT_URL_PREFIX", "")
    _tasks_name = os.environ.get("TASKS")
    if reports_url_prefix and report_file:
        _report_uri = os.path.join(reports_url_prefix, report_file)
    else:
        _report_uri = None

    # TODO(janvondra): Support multinode ES cluster & SSL certificate security
    _es_schema = os.environ.get("RALLY_ELASTIC_SCHEMA", "http")
    _es_hostname = os.environ.get("RALLY_ELASTIC_HOSTNAME")
    _es_port = os.environ.get("RALLY_ELASTIC_PORT", 9200)
    _es_index = os.environ.get("RALLY_ELASTIC_INDEX", "rally")
    _es_user = os.environ.get("RALLY_ELASTIC_USER")
    _es_password = os.environ.get("RALLY_ELASTIC_PASSWORD")

    _fluent_hostname = os.environ.get("RALLY_FLUENT_HOSTNAME")
    _fluent_port = os.environ.get("RALLY_FLUENT_PORT", 24224)
    _fluent_tag = os.environ.get("RALLY_FLUENT_TAG", "openstack.rally_tasks")

    if not debug:
        if _es_hostname:
            if _es_user and _es_password:
                _es_uri = "{}://{}:{}@{}:{}".format(_es_schema, _es_user,
                                                    _es_password, _es_hostname,
                                                    _es_port)
            else:
                _es_uri = "{}://{}:{}".format(_es_schema, _es_hostname,
                                              _es_port)

            index_settings_file = os.path.join(
                os.path.abspath(os.path.dirname(sys.argv[0])),
                "rally_mapping.json")

            _exporter = ElasticExporter(_es_uri, _es_index,
                                        index_settings_file)
        elif _fluent_hostname:
            _exporter = FluentExporter(_fluent_tag, _fluent_hostname,
                                       _fluent_port)
        else:
            LOG.error("No available exporter details!")
            sys.exit(1)

    else:
        _exporter = DummyExporter()

    input_json = sys.stdin.read()
    try:
        parsed_json = json.loads(input_json)
    except ValueError:
        LOG.error("No task report to export: {}".format(input_json))
        sys.exit(1)

    denormalizer = RallyDenormalizer(_exporter, _report_uri, _tasks_name)

    try:
        denormalizer.process_result_json(parsed_json)
        LOG.info("Report successfully sent")
    except Exception as e:
        LOG.exception("Report indexing failed.")
        # LOG.exception("Report indexing failed for data: {}".format(
        #     input_json))
        sys.exit(1)
