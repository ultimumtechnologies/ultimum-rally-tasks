#!/bin/bash

#[compute]
IMAGE_REF=`openstack image list | grep cirros | tr -d " " | cut -d '|' -f2`
IMAGE_REF_ALT=`openstack image list | grep amphora | tr -d " " | cut -d '|' -f2`
if [[ -z $IMAGE_REF_ALT ]]; then
  IMAGE_REF_ALT=$IMAGE_REF
fi
FLAVOR_REF=`openstack flavor list | grep m1.medium | tr -d " " | cut -d '|' -f2`
FLAVOR_REF_ALT=`openstack flavor list | grep m1.large | tr -d " " | cut -d '|' -f2`

#[network]
FLOATING_NETWORK_NAME="public1"
PUBLIC_NETWORK_ID=`openstack network list | grep $FLOATING_NETWORK_NAME | tr -d " " | cut -d '|' -f2`


if [ ! -e to_override_tempest.conf ]; then
  cat <<EOF > to_override_tempest.conf
[DEFAULT]
log_file = /var/log/rally.log

[compute]
image_ref = $IMAGE_REF
image_ref_alt = $IMAGE_REF_ALT
flavor_ref = $FLAVOR_REF
flavor_ref_alt = $FLAVOR_REF_ALT
min_compute_nodes = 2

[compute-feature-enabled]
live_migration = True
vnc_console = False
spice_console = True

[network]
public_network_id = $PUBLIC_NETWORK_ID
floating_network_name = $FLOATING_NETWORK_NAME

EOF
fi
